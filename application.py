import requests

print('Learning git, cool!')
print('just for example')
print('once again example')
print("Some bag was fixed again")

print('new feature was added')

url = "https://www.tury.ru/hotel/most_luxe.php"

def get_data(url):
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 OPR/77.0.4054.203"
    }

    r = requests.get(url=url, headers=headers)

    with open("index.html", "w") as file:
        file.write(r.text)


def main():
    get_data("https://www.tury.ru/hotel/most_luxe.php")

if __name__ == 'main':
    main()
